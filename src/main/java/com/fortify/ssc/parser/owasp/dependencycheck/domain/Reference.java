package com.fortify.ssc.parser.owasp.dependencycheck.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;

@Getter
public class Reference {
    @JsonProperty String source;
    @JsonProperty String url;
    @JsonProperty String name;
}
